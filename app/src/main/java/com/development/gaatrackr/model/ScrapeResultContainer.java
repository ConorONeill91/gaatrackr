package com.development.gaatrackr.model;

import java.util.ArrayList;

/**
 * Created by conor on 04/04/15.
 */
public class ScrapeResultContainer {
    public ArrayList<ScrapeResult> getResults() {
        return results;
    }

    public void setResults(ArrayList<ScrapeResult> results) {
        this.results = results;
    }

    private ArrayList<ScrapeResult> results;

}
