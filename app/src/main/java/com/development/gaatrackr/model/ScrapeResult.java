package com.development.gaatrackr.model;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by conor on 04/04/15.
 */
public class ScrapeResult implements Parcelable {
    private String source;
    private String href;
    private String title;
    private String content;
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ScrapeResult(){}

    // Parcelling
    public ScrapeResult(Parcel in) {
        String [] data = new String[5];
        in.readStringArray(data);
        this.source = data[0];
        this.href = data[1];
        this.title = data[2];
        this.content = data[3];
        this.imageUrl = data[4];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[] {this.source,
                this.href,
                this.title,this.content,this.imageUrl});
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public ScrapeResult createFromParcel(Parcel in) {
            return new ScrapeResult(in);
        }

        public ScrapeResult[] newArray(int size) {
            return new ScrapeResult[size];
        }

    };
}
