package com.development.gaatrackr.scrape;

import com.development.gaatrackr.model.ScrapeResult;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by conor on 04/04/15.
 */
public class HoganStandAdapter implements ScrapeAdapter {

    private static Logger logger = Logger.getLogger(HoganStandAdapter.class.getName());

    @Override
    public String getName(){
        return "HoganStand.com";
    }

    @Override
    public void performScrape(List<ScrapeResult> container) {
        String content = "";
        try {
            Document doc = Jsoup.connect("http://www.hoganstand.com").get();

            Elements results = doc.getElementsByClass("NormalStory");
            for (Element element : results) {
                if (element == null) {
                    continue;
                }
                ScrapeResult result = new ScrapeResult();
                result.setSource(this.getName());
                logger.info(result.getContent());
                result.setHref("http://www.hoganstand.com/" + element.select("a").attr("href"));
                logger.info(result.getContent());
                result.setTitle(element.select("a").text());
                logger.info(result.getContent());
                Document contentDoc = Jsoup.connect(result.getHref()).get();
                Elements contentResults = contentDoc.getElementsByClass("content");
                for (Element elem : contentResults) {
                    content += elem.select("p").text() + " ";
                }
                result.setContent(content);
                logger.info(result.getContent());
                content = ""; // StringBuilder? Immutable..
                logger.info(result.getHref() + " " + result.getTitle() + "\n" + result.getContent());
                container.add(result);
            }
        }
        catch(IOException e){
            logger.info("Error connecting to HS");
        }

    }





}
