package com.development.gaatrackr.scrape;

import com.development.gaatrackr.model.ScrapeResult;

import java.util.List;



/**
 * Created by conor on 04/04/15.
 */
public interface ScrapeAdapter {
    public String getName();

    public void performScrape(List<ScrapeResult> container);
}
