package com.development.gaatrackr.scrape;

import com.development.gaatrackr.model.ScrapeResult;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by conor on 08/04/15.
 */
public class BreakingNewsAdapter implements ScrapeAdapter {

    private static Logger logger = Logger.getLogger(BreakingNewsAdapter.class.getName());

    @Override
    public String getName() {
        return "BreakingNews.ie";
    }

    @Override
    public void performScrape(List<ScrapeResult> container) {
        String content = "";
        try {
            Document doc = Jsoup.connect("http://www.breakingnews.ie/sport/gaa").get();
            Elements results = doc.getElementsByClass("with-preview-pic");
            for (Element element : results) {
                if (element.select("a").size() == 0)
                    continue;

                ScrapeResult result = new ScrapeResult();
                result.setSource(this.getName());
                result.setHref("http://www.breakingnews.ie" + element.select("a").first().attr("href"));
                result.setImageUrl(element.select("img").first().attr("src"));
                result.setTitle(element.select("a").text() + ".");
                logger.info(result.getImageUrl() + "\n" + result.getHref() + " \n " + result.getTitle());
                Document contentDoc = Jsoup.connect(result.getHref()).get();
                Elements contentResults = contentDoc.getElementsByClass("ctx-content");
                for (Element elem : contentResults) {
                    content += elem.select("p").text() + "\n";
                }
                result.setContent(content);
                content = ""; // StringBuilder? Immutable..
                logger.info(result.getContent());


                container.add(result);
            }
        } catch (IOException e) {
            logger.info("Error connecting to Dublin GAA");
        }
    }

    public static void main(String[] args) {
        ArrayList<ScrapeResult> r = new ArrayList<>();
      BreakingNewsAdapter e = new BreakingNewsAdapter();
        e.performScrape(r);
    }
}
