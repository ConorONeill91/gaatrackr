package com.development.gaatrackr.scrape;

import com.development.gaatrackr.model.ScrapeResult;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by conor on 04/04/15.
 */
public class DublinGAAAdapter implements ScrapeAdapter {

    private static Logger logger = Logger.getLogger(DublinGAAAdapter.class.getName());

    @Override
    public String getName() {
        return "DublinGAA.ie";
    }

    @Override
    public void performScrape(List<ScrapeResult> container) {
        String content = "";
        try {
            Document doc = Jsoup.connect("http://www.dublingaa.ie/news").get();
            Elements results = doc.getElementsByClass("small");
            for (Element element : results) {
                if (element.select("a").size() == 0)
                    continue;

                ScrapeResult result = new ScrapeResult();
                result.setSource(this.getName());
                result.setHref("http://www.dublingaa.ie" + element.select("a").first().attr("href"));
                Document contentDoc = Jsoup.connect(result.getHref()).get();
                Elements contentResults = contentDoc.getElementsByClass("panels_wrap");
                for (Element elem : contentResults) {
                    content += elem.select("p").text() + "\n";
                }
                result.setContent(content);
                logger.info(result.getContent());

                logger.info(result.getImageUrl());
                result.setTitle(contentResults.select("h1").first().text());
                logger.info(result.getTitle());
                content = ""; // StringBuilder? Immutable..
                result.setImageUrl("http:" + contentResults.select("img").first().attr("src"));
                logger.info(result.getHref() + " " + result.getTitle() + "\n" + result.getContent());
                container.add(result);
            }
        } catch (IOException e) {
            logger.info("Error connecting to Dublin GAA");
        }
    }



}