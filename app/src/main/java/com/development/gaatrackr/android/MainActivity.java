package com.development.gaatrackr.android;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.internal.widget.AdapterViewCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.development.gaatrackr.R;
import com.development.gaatrackr.model.ScrapeResult;
import com.development.gaatrackr.scrape.BreakingNewsAdapter;
import com.development.gaatrackr.scrape.DublinGAAAdapter;
import com.development.gaatrackr.scrape.HoganStandAdapter;
import com.development.gaatrackr.scrape.ScrapeAdapter;
import com.twotoasters.jazzylistview.JazzyHelper;
import com.twotoasters.jazzylistview.JazzyListView;
import java.util.ArrayList;
import java.util.Collections;


public class MainActivity extends ActionBarActivity {

    private JazzyListView list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("GAATrackR");
        list = (JazzyListView) findViewById(R.id.jazzy_list);
        list.setTransitionEffect(JazzyHelper.ZIPPER);
        new ScrapeTask().execute();

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
                ScrapeResult clickedResult = (ScrapeResult) list.getItemAtPosition(position);
                Intent intent = new Intent(getApplicationContext(),ArticleActivity.class);
                intent.putExtra("ArticleIntent",clickedResult);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class ScrapeTask extends AsyncTask<Void, Void, ArrayList<ScrapeResult>>{

        private ProgressDialog progressDialog;

        // Place progress wheel on screen
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(MainActivity.this, "Nóiméad Amháin",
                    "Serving up your favourite GAA news..");
        }

        // Scrape Websites for data
        @Override
        protected ArrayList<ScrapeResult> doInBackground(Void... params) {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            ArrayList<ScrapeAdapter> adapters = new ArrayList<>();
            ArrayList<ScrapeResult> results = new ArrayList<>();
            adapters.add(new BreakingNewsAdapter());
            adapters.add(new DublinGAAAdapter());
            for(ScrapeAdapter adapter: adapters){
                adapter.performScrape(results);

            }

            return results;
        }
        // Pass scraped data to adapter.
        @Override
        protected void onPostExecute(ArrayList<ScrapeResult> results) {
            progressDialog.cancel();
            Collections.shuffle(results);
            ListAdapter adapter = new ListAdapter(MainActivity.this,R.layout.item_layout,results);
            list.setAdapter(adapter);

        }

    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exit")
                .setMessage("Are you sure you want to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
