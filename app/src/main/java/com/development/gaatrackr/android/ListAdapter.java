package com.development.gaatrackr.android;

import android.content.ClipData;
import android.content.Context;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.development.gaatrackr.R;
import com.development.gaatrackr.model.ScrapeResult;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by conor on 06/04/15.
 */
public class ListAdapter extends ArrayAdapter<ScrapeResult>{

    private ArrayList<ScrapeResult> items;

    public ListAdapter(Context context, int resId,ArrayList<ScrapeResult> items){
        super(context,resId,items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View w = convertView;

        if(w == null){
            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            w = inflater.inflate(R.layout.item_layout,null);
        }

            ScrapeResult scrapeResult = items.get(position);

            ImageView image = (ImageView) w.findViewById(R.id.image);
            TextView title = (TextView)w.findViewById(R.id.title);
            //TextView content = (TextView)w.findViewById(R.id.content);
            TextView source = (TextView)w.findViewById(R.id.source);

            Picasso.with(getContext()).load(scrapeResult.getImageUrl()).resize(275,275).into(image);
            title.setText(scrapeResult.getTitle());
            Log.i("Title" , scrapeResult.getTitle());
            //content.setText(scrapeResult.getContent());
            source.setText("Via " + scrapeResult.getSource());

        return w;
    }


}
