package com.development.gaatrackr.android;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.development.gaatrackr.R;
import com.development.gaatrackr.model.ScrapeResult;
import com.squareup.picasso.Picasso;

public class ArticleActivity extends ActionBarActivity {

    private ImageView articleImage;
    private TextView articleSource;
    private TextView articleTitle;
    private TextView articleContent;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article);
        setTitle("GAATrackR");
        articleImage = (ImageView) findViewById(R.id.article_image);
        articleSource = (TextView) findViewById(R.id.article_source);
        articleTitle = (TextView) findViewById(R.id.article_title);
        articleContent = (TextView) findViewById(R.id.article_content);

        // Intent from Main Act
        Intent intent = getIntent();
        ScrapeResult articleResult = intent.getParcelableExtra("ArticleIntent");

        // Assign Images / Text Fields values
        Picasso.with(getApplicationContext()).load(articleResult.getImageUrl()).resize(1000,500).into(articleImage);
        articleSource.setText("Via " + articleResult.getSource());
        articleTitle.setText(articleResult.getTitle());
        articleContent.setText(articleResult.getContent());

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_article, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
